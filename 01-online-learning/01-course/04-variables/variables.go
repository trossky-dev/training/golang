package main

import "fmt"

/*
*    Recordemos
* Variabe es un espacio en memoria en la que podemos almacenar información.
 */
func main() {

	// Primera Forma de declaración
	var firstVariable string              // Declaración
	firstVariable = " Trossky Developer " // asignamción

	fmt.Println(firstVariable)

	// Segunda Forma de declaración
	var secondVariable = " CEO: Luis Fernando Garcia"
	fmt.Println(secondVariable)

	// Tercera Forma de declaración [REMENDADA]
	thridVariable := " CEO: Luis Fernando Garcia"
	fmt.Println(thridVariable)

}
