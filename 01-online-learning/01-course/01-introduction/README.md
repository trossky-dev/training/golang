![alt text][logo]
# Golang

Es un lenguaje de programacaión Open Source que facilita la creación de software simple, confiable y eficiente.

Go es compilado, concurrente, su sintaxis es similar a C, con tipado estatico.
Tiene un recolector de Basura.

Go tambien admite la programación orientada a objetos. Aunque quizas no parezca.
Ya que no soporta herencia ni tampoco usa palabras claves que identifiquen dicho paradigma.

Aparece en el año 2009 aproximadamente 8 años.

Es un lenguaje relativamente nuevo en el mundo de los lenguajes de programacion, esto quizas hace que no sea
confiable para implementar en los desarrollos de empresas.
Pero lo que hace que Go sea popular es que es desarrollado y matenido por Google, especificamente por el
desarrollador Kent Tomson, uno de los creadores del sistema Unix.

Empresas Y Proyecto que implementan este lenguaje.

Google:
  Youtube
Twitter
BitBucket
Facebook
Basecam
Uber
Docker.



[logo]: http://gygacode.com/wp-content/uploads/2016/02/goraster.png "The Go Programming Language Specification"
