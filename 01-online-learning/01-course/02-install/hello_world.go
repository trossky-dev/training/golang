package main

// alterar los nombres de los paquetes .
import console "fmt"

/**
*               OJO!
* No es recomendable  renombrar paquetes,
* puede generar conflictos o alterar la legibilidad de tu código.
**/

func main() {

	console.Println("Helllo World")
}

/**
*    Cómo correr el programa?
*  implemente corriendo el comando en la raiz del drectorio de tu código.
*   >  go run hello_world.go
**/

/**
*    Cómo compilar el programa?
*  implemente corriendo el comando en la raiz del drectorio de tu código.
*   >  go build hello_world.go
*   >  ./hello_world
**/
