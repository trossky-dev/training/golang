package main

import "fmt"

func main() {

	fmt.Println("\t ** Primera Propiedad MAKE **")

	// Crear un sclice con MAKE

	slice := make([]int, 3, 56)
	fmt.Println("SLICE con MAKE -> ", slice)

	fmt.Println("LOGITUD DEL SLICE -> ", len(slice))

	fmt.Println("\t ** Segunda Propiedad CAP **")

	fmt.Println("CAPACIDAD  DEL SLICE -> ", cap(slice))

	fmt.Println("\t ** FUNCIóN APPEND **")

	slice = append(slice, 6)
	fmt.Println("APPEND  DEL SLICE -> ", slice)

	fmt.Println("\t ** OTROS EJEMPLOS **")

	slice2 := make([]int, 0, 3) // creamos un nuevo slice con make

	fmt.Println("APPEND  DEL SLICE -> ", slice2)

	slice2 = append(slice2, 6) //función append
	fmt.Println("APPEND  DEL SLICE -> ", slice2)

}
