# [Dos Propiedades de SLICES](https://blog.golang.org/go-slices-usage-and-internals)

## MAKE
La estructura del MAKE es:

       make(TIPO,LOGITUD,[CAPACIDAD] )
Primer Elemento:

       un puntero
Segundo Elemento:

       La longitud del sclice
Tercer Elemento:

      Capasidad del Slice

Otro forma:

     make([]TIPO,LOGITUD,[CAPACIDAD] )
Primer Elemento:

       Un Arreglo
Segundo Elemento:

       La longitud del sclice       
Tercer Elemento:

      Capasidad del Slice

## CAPACITING

### En que Afecta que exista una Capasidad y una Longitud?.

## Caso 1: Eficiente
    slice2 := make([]int, 0, 2) // creamos un nuevo slice con make
    slice2 = append(slice2, 6) //función append


## Caso 2: Deficiente
    slice2 := make([]int, 0, 0) // creamos un nuevo slice con make
    slice2 = append(slice2, 6) //función append
### Razones:

Internamente cuando un slice desborda su Capasidad.
La funcion APPEND le extiende la capacidad por una mas grande. Osea recontruye un sclice.

Cuando se tiene una capasidad definida, la función APPEND. Unicamente agrega el elemnto al Slice, sin la nececidad de crear uno nuevo.


[Sitio Web][1]

[1]:https://blog.golang.org/go-slices-usage-and-internals
