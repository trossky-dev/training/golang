package main

import "fmt"

// Interface Y DOS estructuras
type User interface {
	Permissions() int // 1-5
	Name() string     // 1-5

}

// estrucrura numero UNO que implementara la interface
type Admin struct {
	name string
}

func (this Admin) Permissions() int {
	return 5

}

func (this Admin) Name() string {

	return this.name
}

// estrucrura numero DOS que implementara la interface
type Editor struct {
	name string
}

func (this Editor) Permissions() int {
	return 4

}

func (this Editor) Name() string {

	return this.name
}

func auth(user User) string {
	if user.Permissions() > 4 {
		return user.Name() + " Tiene permisos de Administrador"

	} else if user.Permissions() > 3 {
		return user.Name() + " Tiene permisos de Editor"
	}
	return "No tiene permisos"
}

func main() {
	fmt.Println("Manejando Interfaces como Tipo de datos de Arreglos")

	userList := []User{Admin{"Luis Admin"}, Editor{"Trossky Editor"}}

	for _, user := range userList {
		fmt.Println(auth(user))
	}

}
