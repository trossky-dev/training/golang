package main

import "fmt"

// Interface
type User interface {
	Permissions() int // 1-5
	Name() string     // 1-5

}

// estrucrura que implementara la interface
type Admin struct {
	name string
}

func (this Admin) Permissions() int {
	return 5

}

func (this Admin) Name() string {

	return this.name
}

func auth(user User) string {
	if user.Permissions() > 4 {
		return user.Name() + " Tiene permisos de Administrador"

	}
	return "No tiene permisos"
}

func main() {

	fmt.Println("Manejando Interfaces")

	admin := Admin{"Luis Garcia"}

	fmt.Println(auth(admin))
}
