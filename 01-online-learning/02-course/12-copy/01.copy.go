package main

import "fmt"

func main() {
	fmt.Println("\t *******  Función Copy  **** ")
	fuente := []int{1, 2, 3, 4}
	destino := make([]int, 0)

	fmt.Println("Fuente ", fuente)
	fmt.Println("Copia ", destino)

	fmt.Println("\t ******* APlicando la Función Copy  **** ")

	copy(destino, fuente)
	fmt.Println("Copia ", destino)

	fmt.Println("\t ******* Solución para Función Copy  **** ")
	fuenteSolucion := []int{1, 2, 3, 4, 5}
	destinoSolucion := make([]int, len(fuenteSolucion), cap(fuenteSolucion)*2)

	fmt.Println("Fuente ", fuenteSolucion)
	fmt.Println("Copia ", destinoSolucion)

	fmt.Println("\t ******* APlicando la Función Copy  **** ")

	copy(destinoSolucion, fuenteSolucion)
	fmt.Println("Copia ", destinoSolucion)

}
