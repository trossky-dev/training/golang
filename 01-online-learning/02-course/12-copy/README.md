# Función COPY
___

Basicamente, permite copiar  Arreglos.

## La Estructura:

    copy(destino,fuente)

Destino: Arreglo donde se va realizar la copia.

 Fuente: Arreglo que se desea copiar

## Importante

definir el Sclice de Destino con una longitud

La función COPY, copia el minimo de elementos de los dos Arreglos(destino,origen).

Ejemplo:

code
```go
fuente := []int{1, 2, 3, 4}
destino := make([]int, 0)
copy(destino, fuente)
fmt.Println("Copia ", destino)

```
output
```
$ Copia  []
```
Para el ejemplo anterior, el minimo de elementos es Cero.
Por que la longitud del sclice [fuente] es 4. Y el sclice [destino] es 0.

Enclución el numero de elemnentos a copiar es cero[0], osea el minimo de [fuente] y [longitud].

### Una Posible solución.

Una posible solucion es definir la longitud del slice destino, de forma estatica.

Ejemplo:

code
```go
fuenteSolucion := []int{1, 2, 3, 4, 5}
destinoSolucion := make([]int, len(fuenteSolucion))

copy(destino_solucion, fuenteSolucion)
fmt.Println("Copia ", destinoSolucion)

```
output
```
$ Copia  [1 2 3 4 5]
```
### Otra Solución:
Otra solución es al slice [destino] indicarle la capasidad, el doble de la capacidad del [fuente]

code
```go
fuenteSolucion := []int{1, 2, 3, 4, 5}
destinoSolucion := make([]int, len(fuenteSolucion),cap(fuenteSolucion)*2)

```
