package main

import (
	"fmt"
	"strconv"
)

/*
*    Convertir Tipos de Variables
*    strconv //convertir a String
 */
func main() {

	// Integer to String
	age := 22
	ageStr := strconv.Itoa(age)

	fmt.Println("Mi Edad es " + ageStr)

	/************/
	numberStr := "20"
	number, _ := strconv.Atoi(numberStr)

	fmt.Println(age + number)

}
