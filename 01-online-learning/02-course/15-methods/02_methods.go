package main

import (
	"fmt"
)

type _User struct {
	age            int
	name, lastname string
}

// Recibiendo la estructura como una copia
func (user _User) nameComplete() string {
	return user.name + " " + user.lastname

}

// Recibiendo la estructura como un puntero
func (user *_User) setName(n string) {
	user.name = n

}
func main() {

	fmt.Println("Introducción a Métodos en Go")

	ceo := new(_User)

	ceo.name = "Luis"
	ceo.setName("Fernando")
	ceo.lastname = "Garcia"

	fmt.Println(ceo.name)
	fmt.Println(ceo.nameComplete())
}
