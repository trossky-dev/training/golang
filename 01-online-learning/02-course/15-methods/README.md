## Metodos

Son quellos que se ejecutan mediante una variable, con un tipo de dato.




```go
func (this User) name_complete() string{

}

```
 Para agregar una función a una estructura,
 se debe indicar el nombre de la estructura y un identificador.  
  En el caso anterior(this User)  la estructura es: User y el identificador dentro de dicha función es [this].  
  Sigue el nombre de la función. y como ultimo paso se indica el tipo de dato que retornara. En el ejemplo anterior es string.


## Funciones de Structuras

### Como una copia
Pasar la referencia de la estructura a la función una copia, esta crea una nueva estructura
para ser tratadas en esa misma funcion.
```go
func (this User) name_complete() string{

}

```

### Como un puntero de la estructura

Pasar la referencia de la estructura a la función como puntero.
```go
func (this *User) name_complete() string{

}

```
#### Ventajas pasar como puntero
Son mas efcicientes, debido a que es no es lo mismo copias una estrucrura a copiar solo una referencia
en memorias


## NOTA
Solo podemos agrgarle metodos a las estructuras, mientras esteamos en el mismo paquete.
Si estas en otro paquete no puede crearle mas funciones a las estructuras.
