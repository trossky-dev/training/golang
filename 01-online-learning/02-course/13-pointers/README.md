# Punteros en Go

## Que es?

1. Es una dirección de memoria.
Todas las variables estan almacendas en una dirección de memoria.
2. En lugar del valor, tenemos la dirección donde esta el valor.
3. X y Y =>(puntando) #af123d, = (almacenando) 5
4. X =>(puntando) #af123d, = (almacenando) 6
5. Y ¿? => 6

```go
*T => *int,*string,*Struct
```      

## Casos de Uso

Lo mas Importante, es saber cuando usar los punteros.

## El Valor Cero
El Valor Cero de los Punteros es [nil]

## Importante

Para extraer el valor de la memoria, tiene que ser a traves de la una dirección de variable.
```go
entero:=5
x=&entero
```
## Operadores [*][&]
EL [&] extrae la dirección de memoria de una variable.

  EL [* ] Nos da acceso al valor que esta en memoria.


## En Conclución

 Los dos Apuntadores[X,Y] tienen el mismo valor. Aunque solo alteramos un apuntador[x].

###### [RECUERDE]
 Estamos alterando en la direccion de memoria. X y Y tiene la misma direccion de memoria.
