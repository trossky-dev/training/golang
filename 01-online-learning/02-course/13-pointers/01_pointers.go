package main

import "fmt"

func main() {
	fmt.Println("Punteros en Go")

	var x, y *int //Declaración de Punteros en Go
	entero := 5

	fmt.Println("\t *** Asignando Direcciones *** ")
	x = &entero
	y = &entero

	fmt.Printf("Dirección de Memoria de X => %v =>[valor] %v \n", x, *x)
	fmt.Printf("Dirección de Memoria de Y => %v =>[valor] %v \n", y, *y)

	fmt.Printf("\t *** Alterando la direccion de Memoria [%v] *** \n", x)
	*x = 6

	fmt.Printf("Dirección de Memoria de X => %v =>[valor] %v \n", x, *x)
	fmt.Printf("Dirección de Memoria de Y => %v =>[valor] %v \n", y, *y)

	/**
	 *             En Conclución
	 *
	 * Los dos Apuntadores[X,Y] tienen el mismo valor. Aunque solo alteramos un apuntador[x].
	 *
	 * [RECUERDE] => Estamos alterando en la direccion de memoria. X y Y tiene la misma direccion de memoria.
	 */

}
