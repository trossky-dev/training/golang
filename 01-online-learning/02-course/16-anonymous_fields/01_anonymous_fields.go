package main

import "fmt"

type Human struct {
	name string
}

func (this Human) speak() string {
	return "Iniciando la conversación El Robot en Go"

}

type Dummy struct {
	name string
}

type Proffesor struct {
	Human //capo anonimo
	Dummy
}

/*
* Sobreescribiendo el medoto de Human, [speak()]
 */
func (this Proffesor) speak() string {
	return this.Human.speak() + " Y enregando ROL al Robot como Profesor"

}

func main() {

	fmt.Println("Helllo World")

	teacher := Proffesor{Human{"Luis Fernando"}, Dummy{"Garcia"}}

	fmt.Println(teacher.Human.speak())
	fmt.Println(teacher.speak())
}
