package main

import "fmt"

func main() {
	age := 20
	fmt.Printf("Mi edad es: %d \n", age)

	name := "Luis Fernando"
	fmt.Printf("Mi nombre es: %s \n ", name)

	isDeve := true
	fmt.Printf("Usted es desarrollador? %t \n ", isDeve)

	// Imprimir la variable, con el valor por defecto

	fmt.Printf("Mi edad es: %v \t Mi nombre es: %v \t ¿Es usted  desarrollador? %v  \n ", age, name, isDeve)

	// FLOTANTES
	value := 21.12
	fmt.Printf("Valor Flotante %f\n", value) //REMENDADO

	fmt.Printf("Valor Flotante %e\n", value)
	fmt.Printf("Valor Flotante %b\n", value)
}
