# Install Golang

En su sitio web oficial, estan los pasos necesarios para llevar acabo la instalación segun su
sistema operativo.

Pagina Ofial: https://golang.org/


Para el desarrollo de este tutorial usamos como sistema operativo, una distribución basada en
linux. Elementary OS Loki.

Nota: tambien esta probado en Ubuntu 16.04

Una vez instalado y chequeado la versión de Go.
con el siguiente comando:
       go version

salida:
      go version go1.8.1

Y empezaremos con un pequeño escrip, con extención [.go], o sea nuestro primer código fuente
en Golang. Y realizaremos el famoso programa Hola Mundo.

Que IDE usar para Programar en GoLang?
Recomiendo usar editores de texto, en la mayoria de los casos, son mas livianos y rapidos para
iniciar o continuar con tus proyectos.
 Como Sublime Text, Atom, Visual Studio Code.

Si ya estas desarrollando algo mas robusto, quizas pueda seguir con los editores anteriores
o quizas pueda usar alternativas de IDES.

IDES recomendados: Intellij IDEA, agregando su plugin para Go y ya esta.

Pues los IDEA me permiten mayor control de la arquitectura de mi proyecto, como son las
bases de datos, seguimiento de su MicroServios REST que desarrolle, etc.

Usted como usuario y desarrollador, puede tener sus propios practicas de desarrrollo
y evaluar sus alternativas que existen en la web y definir sus herrmientas de desarrollo
que se ajuste a usted. Ojo, no deje que las herramientas hagan que usted se ajuste a ellas.



##  EMPEZEMOS!!!

# Estructura de un Progrma en GO.

Consiste  de Tres Partes
Primero:

Go requiere de un paquete principal [pakage main].
Bueno algo que mensionar aqui, ya que hablamos de Paquetes. Go a diferencia de otros lenguajes como C, esta
compuesto de paquetes no de librerias.
De hecho esto hace que Go sea mas rapido que C.

Segundo:
De igual forma que en el aterior, es necesario  crear una función  igual al nombre del
paquete principal. Osea una función MAIN().

Tercera Parte:
La parte en la que vamos a importar distintos apquetes.
en nuestro caso vamos a usar el paquete ["fmt"], que nos va permitir mostrar mensajes o recibir mensajes, en otros terminos, nos  permitira leer entradas o imprimir salidas de  nuestro programa.
