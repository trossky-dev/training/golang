package main

import (
	"fmt"
)

// a

type User struct {
	age            int
	name, lastname string
}

func main() {
	fmt.Println("\t *** Estructuras ***")
	// Crear variable del tipo User
	fmt.Println("Forma 1 de Declarar variables con un nuevo tipo de dato")

	var luis User
	fmt.Println("Variable =>", luis)

	fmt.Println("Forma 2 de Declar variables con un nuevo tipo de dato")

	ceo := User{name: "Luis", lastname: "Garcia"}
	fmt.Println("Variable =>", ceo)

	fmt.Println("Forma 3 de Declar variables con un nuevo tipo de dato")

	trossky := User{1, "Trossky", "Dev"}
	fmt.Println("Variable =>", trossky)

	fmt.Println("Forma 4 de Declar variables con un nuevo tipo de dato usado [NEW] ")

	fernando := new(User)
	fmt.Println("Variable =>", fernando)

	fmt.Println("Accediendo a los campos de una estructura ")
	fmt.Println("User =>", ceo.name)

	fmt.Println("Cambiando el valor a los campos de una estructura ")
	ceo.name = "Fernando"

	fmt.Println("User =>", ceo.name)
}
