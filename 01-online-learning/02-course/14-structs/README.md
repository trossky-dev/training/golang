# Estructuras.

Aparte de los datos que ya estan definidos por el lenguaje, es necesario trabajar con tipos de datos propios para trabajar en el lenguaje con ese nuevo tipo de dato.

## Como se construye.


```go
type nameStruct struct {
	...
  //podemos enlistar los atributos que va tener ese tipo de dato.

}

```

## Forma de Crear una variable del nuevo tipo.
```go

  var variable nameStruct

```

## Puntero a una estructura.

Cuando tenemos un puntero a una estructura,podemos acederdirectamente a los atributos
```go

  var ceo nameStruct
  fmt.Println("User =>", ceo.name)

```

## Estructuras Mutables.

 Esto permite que se pueda modificar los valores de los campos, despues de haber instanciado

```go
  var ceo nameStruct
  fmt.Println("User =>", ceo.name)

```

Las estructuras son mutables, lo que nos permite modificar los valos de los campos una vez se haya instanciado.
