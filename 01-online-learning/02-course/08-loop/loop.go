package main


import (
  "fmt"
)
func main() {
    /***          CASO 1    **/
   // con los tres elementos, el FOR tradicional
   fmt.Println("Ciclo For con los tres Elemento")
    for i := 0; i < 10; i++ {

      fmt.Printf("El número de iteración es: %v \n",i)

    }

    /***          CASO 2    **/
    fmt.Println("Ciclo For el elemento del centro o condicional, Simulando el WHILE")
    // Solo con el elemento central, tipo Whilw
    i:=0
    for i<10{

      fmt.Printf("El número de iteración es: %v \n",i)
      i++

    }

    /***          CASO 3    **/
    fmt.Println("Ciclo INFINITO")
    // Ciclo infinito
    j:=0
    for {

      if j==2{
        j++
        continue
      }
        fmt.Printf("El número de iteración es: %v \n",j)
      j++

      if j>10 {
        break   // termina el ciclo por completo
      }
    }
}
