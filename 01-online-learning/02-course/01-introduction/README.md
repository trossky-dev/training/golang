
![alt text][logo]
# Golang

Es un lenguaje de programacaión Open Source que facilita la creación de software simple, robusto y eficiente.

Go es compilado, concurrente, su sintaxis es similar a C, con tipado estatico.
Tiene un recolector de Basura.

Go tambien admite la programación orientada a objetos. Aunque quizas no parezca.
Ya que no soporta herencia ni tampoco usa palabras claves que identifiquen dicho paradigma.

Surge en el año 2009 aproximadamente 8 años.

Video de Golang, su evolución, usando gource(https://www.youtube.com/watch?v=4ZM3XCHxE3A).

Es un lenguaje relativamente nuevo en el mundo de los lenguajes de programacion, esto quizas hace que no sea
confiable para implementar en los desarrollos de empresas.
Pero lo que hace que Go sea popular es que es desarrollado y matenido por Google, especificamente por el
desarrollador Kent Tomson, uno de los creadores del sistema Unix.

Empresas Y Proyecto que implementan este lenguaje.

Google:
  Youtube
Twitter
BitBucket
Facebook
Basecam
Uber
Docker.

# Por que usar GO?

La sintanis es clara y fuertemente estandarizada.
  Permitiendo al desarrollor con mas facilidad mantener código de otra persona.
  permitiendo facilmente integrarte a equipo de desarrollo.

El compildor forza que sigas buenas prácticas.

El compilador es Muy rápido.

Soporta concurrencia y facil de manejar.
 La sintaxis es muy sencilla. que thread en java.

Go tiene una paquete HTTP para iniar un servidor web de forma sencilla.

Performance Alto.

Facil de Apreder.

Facil de subir a producción.


# Pora que usar GO?

1. CommandLine Tools
CLi Tool. COsas que usamos en la terminal para optimizar procesos, como
respaldos de bbases de datos.

Infraestrura en el Backend

Herrmientas Internas de software como lo usa Facebook.


Servicos Web

Herramientas de automatización.


Ejemplos.
1. Twitter Explicando como manejan 5 billones de sesiones al dia en tiempo real con Golang.
https://blog.twitter.com/2015/handling-five-billion-sessions-a-day-in-real-time


2.  Como fuimos de 30 servidores a 2 con go iron.io
https://www.iron.io/how-we-went-from-30-servers-to-2-go/

3. Por que Timehop escogió Go para reemplazar su aplicación en rails?.
No solo habla por que go es mejor que rails. En particular habla de una gama de lenguajes que tenian para escoger, por que se quedaron con Go.
Ademas habal de la necesidad de hacer en su lataforma Querys e manera concurrente.
https://medium.com/building-timehop/why-timehop-chose-go-to-replace-our-rails-app-2855ea1912d

4. Herremienta para escalar MySql:
Es una herrmienta que desarrollo Youtube para escalar Mysql, de forma sencilla.
https://github.com/youtube/vitess


[logo]: http://gygacode.com/wp-content/uploads/2016/02/goraster.png "The Go Programming Language Specification"
