package main

/**
*  Tipos de Condicionales
*  if
*  else if
*  else
*/

/**
*  Operadores Logicos!
*  == igual a...
*  != diferente de...
*  > menor que...
*  >= menor o igual a...
*  < mayor que...
*  <= mayo o igual a...
*  && AND
*  || OR
*/
import (
  "fmt"
)
func main() {
  x:=12
  y:=13

  // La llave debe ir en la misma linea del condicional, al contrario genera Error

  if x>y { // los parenticis pueden ir o no!,
    fmt.Printf(" %d Es mayor que %d \n",x,y)
  }else if x<y {
    fmt.Printf(" %d Es menor que %d \n",x,y)
  }else{
    fmt.Printf(" %d Es igual a %d \n",x,y)
  }

}
