package main

import "fmt"

func main() {
	fmt.Println("SLICES EN GOLANG")
	// Los valores CEROS de un SLICE es NULL.
	var slice []int
	slice = []int{1, 2, 3, 44, 20, 5, 9, 10, 8, 7, 3, 6, 3}

	if slice == nil {
		fmt.Println("Esta Vacio")
	} else {
		fmt.Println(slice)
		fmt.Printf("Logitud del Slice %d \n", len(slice))
	}

	// Puntero al areglo

	// Creamos un arreglo normal
	arreglo := [3]int{1, 2, 3}
	fmt.Println("Arreglo: ", arreglo)

	// Convertimos el arrelo en un Slice
	slice2 := arreglo[:2]

	fmt.Println("Arregloconvertido a Slice: ", slice2)

	slice2 = arreglo[1:2]

	fmt.Println("Arregloconvertido a Slice: ", slice2)

	slice2 = arreglo[1:]

	fmt.Println("Arregloconvertido a Slice: ", slice2)

	slice2 = arreglo[0:]

	fmt.Println("Arregloconvertido a Slice: ", slice2)

	slice2 = arreglo[:3]

	fmt.Println("Arregloconvertido a Slice: ", slice2)
}
