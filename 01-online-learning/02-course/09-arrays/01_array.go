package main

import "fmt"

/**
*       Los Arreglos en Golang.
*  Son una colección de datos de un mismo tipo. (Go tipado estatico)
*  Una vez, definido el arreglo, se inicializa con los valores ceros del tipo de dato
*  de este arreglo.
 */
func main() {
	fmt.Println("Arreglos")

	array := [3]int{1, 2}

	fmt.Println(array)

	// función  LEN()
	for i := 0; i < len(array); i++ {
		fmt.Printf("Posición [%d] con valor (%d) \n", i, array[i])
	}

	array[2] = 50
	for i := 0; i < len(array); i++ {
		fmt.Printf("Posición [%d] con valor (%d) \n", i, array[i])
	}

	/**
	*      MATRICES
	* Areglos multidimensionales
	*
	**/
	fmt.Println("Matrices")
	var matriz [6][4]int

	matriz[0][1] = 234

	fmt.Println(matriz)
}
